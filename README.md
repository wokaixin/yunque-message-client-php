### 云雀消息服务-消息推送业务实现-java版
作者 yichen

email 2782268022@qq.com

### 安装
```angular2html
git clone https://gitee.com/wokaixin/yunque-message-client-java.git
```
### 或
```angular2html
composer update 
```
### 长连接主服务
云雀主服务 websocket 链接服务端地址： https://gitee.com/wokaixin/yunque-socket.git
### 其他语言推送支持
云雀推消息业务实现 php版 https://gitee.com/wokaixin/yunque-message-client-php

云雀推消息业务实现 java版 https://gitee.com/wokaixin/yunque-message-client-java

云雀推消息业务实现 go版 https://gitee.com/wokaixin/yunque-message-client-go

### 环境依赖
composer.json 添加如下：

"google/protobuf": "v3.24.1",

 "ext-redis": "*"

### 推送方法 
    /*
     * 推送消息给指定用户
     */
    public static function sendTo(string $uid, MessageProto $msg): bool
    /*
     * 推送消息给指定用户 批量
     */
    public static function sendTos(string $uid, array $msgs): bool
    /*
     * 推送消息给指定多个用户
     * 多个uid 逗号分割
     * params $uids=uid1,uid2,uid3
     */
    public static function sendToUids(string $uids, array $msgs): bool
    /*
     * 推送消息给指定用户的某个设备
     */
    public static function sendToDid(string $uid, string $did, MessageProto $msg): bool
    /*
     * 推送批量消息给指定用户的某个设备
     */
    public static function sendToDids(string $uid, string $did,array $msgs): bool
    /*
     * 推送消息给所有人
     * @params $msg =MessageProto
     */
    public static function sendToAll( MessageProto $msg): bool
    /*
     * 推送消息给所有人
     * @params $msg =MessageProto
     */
    public static function sendToAlls( array $msgs): bool
    /*
     * 遍历消息推给接收人
     * @params $msg =[MessageProto]
     */
    public static function sendList( array $msg): bool
### 使用:
```angular2html
    $msg = new \yunque\client\proto\MessageProto();
    $msg->setId(1);
    $msg->setReceiver("1074");
    $msg->setSender($uid);
    $msg->setContent(["text" => "挺好的"]);
    $msg->setSendTime(time());
    $bool=\yunque\client\service\MessageService::sendTo("1078",$msg);
    if($bool){
        echo("推送成功")
    }
```

### 推送消息示例

所有方法在 yunque\client\service\MessageService.php;
使用时候继承即可。
如下 使用方式
你的应用里的Yunque\Client\test\MessageServiceTest.php 继承了Yunque\Client\service\MessageService.php;
```angular2html
<?php
include "vendor/autoload.php";

use Yunque\Client\service\MessageService;
use Yunque\Client\test\MessageServiceTest;
//自定义服务参考 Yunque\Client\test\MessageServiceTest 的实现逻辑。
// 实现方式1
$ms =new MessageServiceTest();
$msg=new \Yunque\Client\Proto\MessageProto();
$msg->setReceiver("1074");
$msg->setSender("1000");
$msg->setSendTime(time());
$msg->setExtra(['chattype'=>"group","groupId"=>"abc"]);
$msg->setContent(["text"=>"你好"]);
$res=$ms->sendTo("123456",$msg);
echo "test 1 res:".$res;
// 实现方式2
$res=$ms->redisSend($msg);

echo "\ntest 2 res:".$res;

// 实现方式3
$res=$ms->kafkaSend($msg);

echo "\ntest 3 res:".$res;

// 实现方式4
$m2=MessageServiceTest::init(null,"redis");
//$m2=MessageServiceTest::init(null,"kafka");
$res=$m2->sendTo("123456",$msg);

echo "\ntest 4 res:".$res;
//$m2=MessageService::init(null,"kafka");
//$res2=$m2->sendTo("123456",$msg);

 
```

## 下面是关于php protobuf的相关内容

### 路径需要注意，如果修改路径报错，需要重新生成proto文件。
推送消息服务更多接口参考service 目录具体方法实现

如果修改路径 需要修改composer.json文件
```angular2html
"autoload": {
"psr-4": {

#添加一行映射
           "Yunque\\": "Yunque/",
},
```

然后执行更新

composer 添加一行 引用
"google/protobuf": "v3.24.1"
这个版本要和生成工具 protoc版本一致，否则容易有问题。
然后更新
```
composer update
```

Yunque/Client目录下 执行
```angular2html
protoc --php_out= ../ MessageProto.proto
```

proto模版文件如下
>Yunque/Client/GPBMetadata/MessageProto.proto
```angular2html
syntax = "proto3";
//option go_package = "./;ProtoModel";
package Yunque.Client.proto;
option php_metadata_namespace = "Yunque/Client/GPBMetadata";
message MessageProto {
//   消息id必须
   int64 id = 1;
//   操作方法指令可选
   string action = 2;
//   消息内容必选
   map<string,string>  content = 3;
//   发送人uid
   string sender = 4;
//   接收人uid
   string receiver = 5;
//   拓展 可选
   map<string,string> extra = 6;
//   标题 可选
   string title = 7;
//   发送时间 比选 秒时间戳
   int64 sendTime = 8;
//   图片地址 可选
   string image=9;
}
```

### 推送业务/回复请求

使用方法test示例:
test\ReplyBodyServiceTest.php

具体实现逻辑参考:
service/ReplyBodyService.php
```angular2html
    /*
     * 推送消息给指定用户
     */
    public function sendTo(string $uid, ReplyBodyProto $msg): bool

    /*
     * 推送消息给指定用户 批量
     */
    public function sendTos(string $uid, array $msgs): bool

    /*
     * 推送消息给指定多个用户
     * 多个uid 逗号分割
     * params $uids=uid1,uid2,uid3
     */
    public function sendToUids(string $uids, array $msgs): bool

    /*
     * 推送消息给指定用户的某个设备
     */
    public function sendToDid(string $uid, string $did, ReplyBodyProto $msg): bool

    /*
     * 推送批量消息给指定用户的某个设备
     */
    public function sendToDids(string $uid, string $did, array $msgs): bool

    /*
     * 推送消息给所有人
     * @params $msg =ReplyBodyProto
     */
    public function sendToAll(ReplyBodyProto $msg): bool

    /*
     * 推送消息给所有人
     * @params $msg =ReplyBodyProto
     */
    public function sendToAlls(array $msgs): bool

    /*
     * 遍历消息推给接收人
     * @params $msg =[ReplyBodyProto]
     */
    public function sendList(array $msg): bool

```