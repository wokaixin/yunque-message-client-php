<?php
include "vendor/autoload.php";

use Yunque\Client\service\MessageService;
use Yunque\Client\test\MessageServiceTest;
//自定义服务参考 Yunque\Client\test\SendServiceTest 的实现逻辑。
// 实现方式1 默认根据默认配置的驱动推送
$ms =new MessageServiceTest();
$msg=new \Yunque\Client\Proto\MessageProto();
$msg->setReceiver("1074");
$msg->setSender("1000");
$msg->setSendTime(time());
$msg->setExtra(['chattype'=>"group","groupId"=>"abc"]);
$msg->setContent(["text"=>"你好"]);
$res=$ms->sendTo("123456",$msg);
echo "test 1 res:".$res;
// 实现方式2 手动redis 推送
$res=$ms->redisSend($msg);

echo "\ntest 2 res:".$res;

// 实现方式3 手动指定 kafka推送
$res=$ms->kafkaSend($msg);

echo "\ntest 3 res:".$res;

// 实现方式4 手动指定redis 推送
$m2=MessageServiceTest::init(null,"redis");
//$m2=SendServiceTest::init(null,"kafka");
$res=$m2->sendTo("123456",$msg);

echo "\ntest 4 res:".$res;
//$m2=MessageService::init(null,"kafka");
//$res2=$m2->sendTo("123456",$msg);

// 推送业务请求、回复

$reply=new \Yunque\Client\Proto\ReplyBodyProto();
$reply->setCode("200");
$reply->setData(["type"=>"pay","orderid"=>"12312313123"]);
$reply->setReceiver("1074");
$reply->setTimestamp(time());
$m3=\Yunque\Client\test\ReplyBodyServiceTest::init(null,"redis");
$res=$m3->sendTo("123456",$reply);
echo "\ntest 5 res:".$res;
// 实现方式2 手动redis 推送
$res=$m3->redisSend($reply);

echo "\ntest 6 res:".$res;

// 实现方式3 手动指定 kafka推送
$res=$m3->kafkaSend($reply);

echo "\ntest 7 res:".$res;