<?php

namespace Yunque\Client\driver;

class Redis implements Driver
{
    public \Redis $client;

    public  static  function init(array $config = null): Redis
    {
        $self= new Redis();
        $client = new \Redis();
        $client->connect($config['host'], $config['port']);
        $client->auth([$config['password']]);
        $client->select($config['select']);
        $self->client = $client;
        return $self;
    }

    public function sendTo(string $channel, $msg)
    {
        try {
            return $this->client->publish(
                $channel,
                $msg);
        } catch (\RedisException $e) {
            return 0;
        }
    }
}