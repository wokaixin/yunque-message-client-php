<?php

namespace Yunque\Client\driver;

Interface Driver
{
    public function sendTo(string $channel, $msg);
}