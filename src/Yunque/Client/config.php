<?php

namespace Yunque\Client;
class Config
{
    public  static function init()
    {
        return [// 默认缓存驱动
            'default' => 'kafka',
            // 推送驱动配置
            'driver' => [// redis缓存
                'redis' => [#   订阅主题
                    'channelName' => 'message',
                    // 服务器地址
                    'host' => '127.0.0.1',
                    //端口
                    'port' => '6379',
                    // 密码
                    'password' => '123456',
                    //库索引
                    'select' => '3',
                    'timeout' => 0,
                    'expire' => 0,
                    'persistent' => false,
                    ],
                'kafka' => [
                    # 订阅主题
                    'topic' => 'message',
                    // 服务器地址
                    'brokerList' => '127.0.0.1:9092',
                    'metadataRefreshIntervalMs' => 10000,
                    'isAsyn' => false,
                    'brokerVersion' => "0.10.2.1",
                    'requiredAck'=>1,
                    'produceInterval'=>500
                ],
                ],
            ];
    }
}