<?php

namespace Yunque\Client\test;

use Yunque\Client\Config;
use Yunque\Client\service\MessageService;
use Yunque\Client\service\ReplyBodyService;

class ReplyBodyServiceTest extends ReplyBodyService
{
//    继承配置方法，自定义配置
    public function initConfig(): array
    {
        return   Config::init();
    }
    public function redisSend(\Yunque\Client\Proto\ReplyBodyProto $msg){
        $ms=ReplyBodyService::init(null,"redis");
        $res=$ms->sendTo("123456",$msg);
        print_r($res);
        return $res;
    }
    public function kafkaSend(){
        $msg=new \Yunque\Client\Proto\ReplyBodyProto();
        $msg->setReceiver("1074");
        $msg->setTimestamp(time());
        $msg->setData(['auth'=>"pay","orderId"=>"abc123456489978"]);
        $msg->setCode("200");
        $ms=ReplyBodyService::init(null,"kafka");
        $res=$ms->sendTo("123456",$msg);
        return $res;
    }
}