<?php

namespace Yunque\Client\test;

use Yunque\Client\Config;
use Yunque\Client\proto\MessageProto;
use Yunque\Client\service\MessageService;

class MessageServiceTest extends MessageService
{
//    继承配置方法，自定义配置
    public function initConfig(): array
    {
        return   Config::init();
    }
    public function redisSend(\Yunque\Client\Proto\MessageProto $msg){
        $ms=MessageService::init(null,"redis");
        $res=$ms->sendTo("123456",$msg);
        print_r($res);
        return $res;
    }
    public function kafkaSend(){
        $msg=new \Yunque\Client\Proto\MessageProto();
        $msg->setReceiver("1074");
        $msg->setSender("1000");
        $msg->setSendTime(time());
        $msg->setExtra(['chattype'=>"group","groupId"=>"abc"]);
        $msg->setContent(["text"=>"你好"]);
        $ms=MessageService::init(null,"kafka");
        $res=$ms->sendTo("123456",$msg);
        return $res;
    }
}